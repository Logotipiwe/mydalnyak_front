export function objToGet(get_data: networkObj) {
    const getArr: string[] = [];
    let getStr: string;
    if (Object.keys(get_data).length) {
        for (let key in get_data) {
            if (get_data.hasOwnProperty(key)) {
                let value = get_data[key as keyof networkObj];
                if(typeof value === 'object' && value !== null){
                    getArr.push(...Object.keys(value).map((key2)=>{
                        // @ts-ignore
                        return `${key}[${(!isNaN(Number(key2)) ? '' : key2)}]=${encodeURIComponent(value[key2])}`;
                    }));
                } else {
                    if (value !== undefined) getArr.push(key + '=' + encodeURIComponent((value !== null) ? value : 'null'))
                }
            }
        }
        getStr = '?' + getArr.join('&');
    } else getStr = '';
    return getStr;
}