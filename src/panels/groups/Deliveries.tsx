import {inject, observer} from "mobx-react";
import React from "react";
import RootStore from "../../stores/RootStore";
import {
    Button,
    FormItem,
    FormLayout,
    Group,
    Header,
    Input,
    ModalPage,
    ModalPageHeader,
    ModalRoot,
    View
} from "@vkontakte/vkui";
import AppPanelHeader from "./Services/AppPanelHeader";
import {Icon28LocationOutline} from "@vkontakte/icons";
import DeliveriesPanel from "./Delivery/DeliveriesPanel";
import DeliveryModal from "./Delivery/DeliveryModal";

@inject("RootStore")
@observer
class Deliveries extends React.Component<{ RootStore?: RootStore, id: activeStories }, any> {
    componentDidMount() {
        this.props.RootStore!.deliveryStore.onDeliveriesInit();
    }

    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.deliveryStore;

        const header = <AppPanelHeader tabs={[
            rootStore.getTabObj("deliveries", "Доставки", store.fetchDeliveries)
        ]} store={store} icon={<Icon28LocationOutline/>}/>

        let modal;
        if(store.selectedDeliveryId || store.buyingOrderId){
            const delivery = store.getDeliveryById(store.selectedDeliveryId);
             modal = <ModalRoot activeModal={store.buyingOrderId ? "buyingOrder" : "orders"} onClose={store.onModalClose}>
                <ModalPage
                    id={"orders"}
                    header={<ModalPageHeader>Доставка: {delivery?.place}</ModalPageHeader>}
                    dynamicContentHeight
                >
                    <DeliveryModal delivery={delivery!} id={"orders"}/>
                </ModalPage>
                 <ModalPage
                     id={"buyingOrder"}
                     header={<ModalPageHeader>
                         Заказ: {store.buyingOrder?.title} {store.buyingOrder?.amount} шт.</ModalPageHeader>}>
                     <FormLayout>
                         <FormItem top={"Цена"} bottom={"Ваша наценка будет учтена позже"}>
                             <Input value={store.buyingOrderInputPrice}
                                onChange={e=>store.setBuyingOrderInputPrice(e.target.value)}
                             />
                         </FormItem>
                         <FormItem>
                             <Button
                                 mode={"commerce"}
                                 stretched
                                 onClick={()=>store.buyOrder(store.buyingOrder)}
                             >Куплено</Button>
                         </FormItem>
                     </FormLayout>
                 </ModalPage>
            </ModalRoot>;
        }

        return (
            <View activePanel={"deliveries"} modal={modal}>
                <DeliveriesPanel id={"deliveries"} header={header}/>
            </View>
        );
    }
}

export default Deliveries;