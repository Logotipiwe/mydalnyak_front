import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import {Button, Cell, Div} from "@vkontakte/vkui";
import React from "react";
import {Icon16Cancel, Icon16CheckCircleOutline} from "@vkontakte/icons";

@inject("RootStore")
@observer
class OrderItem extends React.Component<{ RootStore?: RootStore, order: order }, any> {
    render() {
        const store = this.props.RootStore!.sellsStore;
        const order = this.props.order;
        return (
            <Cell
                key={order.id}
                after={<>
                    <Button mode={"destructive"} onClick={store.updateOrder.bind(null, order, "cancel")}>
                        <Icon16Cancel/>
                    </Button>
                    <Button mode={"commerce"} onClick={store.updateOrder.bind(null, order, "accept")}>
                        <Icon16CheckCircleOutline/>
                    </Button>
                </>}
                description={order.status}
            >
                {order.id.substr(0,4)}. {order.orderProducts.reduce((sum, pr) => sum + pr.product.price * pr.amount, 0)}p
                <Div>{order.orderProducts.map((orprod, i) => <div>
                    {i + 1}) {orprod.product.title} ({orprod.amount} шт.)
                </div>)}</Div>
            </Cell>
        );
    }
}

export default OrderItem;