import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import React, {ReactNode} from "react";
import {Avatar, Button, Cell, RichCell} from "@vkontakte/vkui";
import DeliveryOrder from "../../../stores/models/DeliveryOrder";
import {
    Icon12Cancel,
    Icon12Market,
    Icon12Verified,
    Icon16MoneyCircle,
    Icon16ShoppingCartOutline,
    Icon16Verified
} from "@vkontakte/icons";

@inject("RootStore")
@observer
class DeliveryOrderItem extends React.Component<{ RootStore?: RootStore, o: DeliveryOrder, key: any }, any> {
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.deliveryStore;
        const order = this.props.o;
        let text: ReactNode = "";
        let caption: ReactNode = order.description;
        let action: ReactNode;
        switch (order.deliveryOrderStatus) {
            case "PUBLISHED":
                text = <span>Ожидает принятия...</span>;
                action = <Button
                    mode={"commerce"}
                    onClick={()=>store.acceptOrder(order)}
                ><Icon16Verified/></Button>;
                break;
            case "ACCEPTED":
                text = <span style={{color: "green"}}>Принят, ожидайте...</span>
                action = <Button
                    mode={"commerce"}
                    onClick={()=>store.openBuyOrderModal(order)}
                ><Icon16ShoppingCartOutline/></Button>;
                break;
            case "REJECTED":
                text = <span style={{color: "red"}}>Отклонён</span>
                caption = "";
                break;
            case "BOUGHT":
                text = <span style={{color: "green"}}>Куплен: {order.price}p</span>
                caption = "Курьер вышлет вам чек"
                // action = <Button
                //     mode={"commerce"}
                //     onClick={()=>store.finishDelivery()}
                // ><Icon16MoneyCircle/></Button>
                break;
            // case "NOT_IN_STORE":
            //     break;
        }
        return (
            <RichCell
                before={<Avatar src={rootStore.getUserById(order.owner_id)?.vkData?.photo_200}/>}
                text={text}
                caption={caption}
                after={<>
                    <span>{order.amount} шт.</span>
                </>}
                actions={<>
                    {order.isBought ? null : <Button
                        mode={"destructive"}
                        onClick={() => store.rejectOrder(order)}
                    ><Icon12Cancel/></Button>}
                    {action}
                </>}
            >{order.title}</RichCell>
        );
    }
}

export default DeliveryOrderItem;