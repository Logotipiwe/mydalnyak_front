/// <reference types="node" />
/// <reference types="react" />
/// <reference types="react-dom" />

declare namespace NodeJS {
    interface ProcessEnv {
        readonly NODE_ENV: 'development' | 'production' | 'test';
        readonly PUBLIC_URL: string;
    }
}

type networkTypesSingle = [string, number, boolean, null, undefined];
type networkTypes = networkTypesSingle[number] | networkTypes[];

type Undefindable<T> = T | undefined;

type networkObj = {
    [x: string]: networkTypes | networkObj | networkObj[]
}
type networkRes = networkObj | networkObj[string];

type activeStories = "sells" | "services" | "delivery" | "profile";

//------backend types--------
type crnkAns<T> = {
    content: T
}
type user = {
    vkId: number,
    room: number | null,
    roles: { title: string }[],
    vkData?: { name: string, lastName: string, photo_200: string },
    paymentData: string
}
type category = {
    title: string,
    id: string
}
type ownable = {
    id: string
    title: string,
    photo: string | null,
    description: string | null,
    owner_id: number,
    createdDate: string
}
type product = ownable & {
    stockAmount: number,
    price: number,
    category: category | null
    status: string
}
type cartItem = { id: string, amount: number };
type orderProduct = {
    product: product,
    amount: number,
    id: string
}
type order = ownable & {
    orderTime: string,
    status: OrderStatus,
    orderProducts: orderProduct[]
}
type subscription = {
    id: string
    subscriberId: number,
}
type newProdSub = subscription & { sellerId: number }
type admissionSub = subscription & { product: { id: string } }

type newProdData = { title: string, price: string }

enum OrderStatus {
    PUBLISHED,
    CANCELLED,
    ACCEPTED,
    FINISHED
}

type rating = {
    ownableId: string
    rating: number | null
    amIVoted: boolean
    myRating: number
    ratingsCount: number
}
type ServiceStatus = "OPENED" | "HIDDEN"

type service = ownable & { price: number, status: ServiceStatus };

type serviceRequest = ownable & { services: service[], status: requestStatus };

type requestStatus = "OPENED" | "FINISHED"

type addingService = { title: string, price: string, description: string }

type tabObj = { key: string, title: string, counter?: number, onSelect: () => any }

type storeWithTabs = { selectedTab: string, changeTab: (val: string) => void }

type newDeliveryData = {
    place: string,
    time: string,
    stopAcceptOrdersTime: string,
    markup: string
}

type delivery = ownable & {
    time: string
    deliveryStatus: deliveryStatus
    stopAcceptOrdersTime: string
    place: string
    price?: number,
    deliveryOrderList: deliveryOrder[]
}

type deliveryOrder = ownable & {
    price?: number
    deliveryOrderStatus: deliveryOrderStatus
    amount: number
}

type addingDeliveryOrder = {
    title: string
    amount: string,
    description: string
}

type deliveryStatus = "PUBLISHED" | "DELIVERED" | "PROCESSING" | "CANCELLED" | "BOUGHT"
type deliveryOrderStatus = "PUBLISHED" | "ACCEPTED" | "REJECTED" | "BOUGHT" | "NOT_IN_STORE"
