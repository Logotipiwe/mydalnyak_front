import {inject, observer} from "mobx-react";
import RootStore from "../../stores/RootStore";
import React from "react";
import UpperPanel from "./UpperPanel";
import {Panel, View} from "@vkontakte/vkui";

@inject("RootStore")
@observer
class Profile extends React.Component<{ RootStore?: RootStore, id: activeStories }, any>{
    render() {
        return (
            <View activePanel={"profile"}>
                <Panel id={"profile"}>
                    <UpperPanel/>
                </Panel>
            </View>
        );
    }
}
export default Profile;