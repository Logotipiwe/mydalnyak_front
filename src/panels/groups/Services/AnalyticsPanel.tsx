import {inject, observer} from "mobx-react";
import React, {ReactNode} from "react";
import RootStore from "../../../stores/RootStore";
import {Button, Cell, Div, Group, InfoRow, Input, Panel, Search, SimpleCell, Spinner, Textarea} from "@vkontakte/vkui";
import Header from "@vkontakte/vkui/dist/components/Header/Header";
import ServiceItem from "./ServiceItem";
import ServicesPanelHeader from "./AppPanelHeader";

@inject("RootStore")
@observer
class ServicesPanel extends React.Component<{ RootStore?: RootStore, id: string, header: ReactNode }, any> {
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.servicesStore;
        return (
            <Panel id={"analytics"}>
                {this.props.header}
                <Div>
                    <Group>
                        <SimpleCell>
                            <InfoRow header={"Заработано на услугах"}>
                                {store.myIncome}p
                            </InfoRow>
                        </SimpleCell>
                    </Group>
                </Div>
            </Panel>
        );
    }
}

export default ServicesPanel;