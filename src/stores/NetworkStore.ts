import {objToGet} from "../Functions";
import RootStore from "./RootStore";
import {makeAutoObservable} from "mobx";

export default class NetworkStore{
    constructor(rootStore: RootStore) {
        makeAutoObservable(this);
        this.rootStore = rootStore;

        this.doAjax = this.doAjax.bind(this);
    }

    rootStore: RootStore;

    doAjax<T extends networkRes>(
        route: string,
        data: networkObj,
        options: any = {},
        resolver: (res: any) => void,
        isThrowError: boolean
    ): Promise<any> {

        let sign = window.location.href.replaceAll('151234421', this.rootStore.inputUserId || "");
        const additionalHeaders: any = {
            "X-sign": sign
        }

        if(
            !options.dontSetContentType
            && ["POST", "PUT", "DELETE"].includes((options.method || "").toString())
            && !(options.headers && options.headers["Content-Type"])
        ){
            additionalHeaders['Content-Type'] = 'application/json';
        }

        options.headers = Object.assign((options.headers || {}), additionalHeaders);

        let requestUrl = this.rootStore.url + route + objToGet(data);

        return fetch(requestUrl, options).then(res => {
            // if(res.status.toString()[0] !== "2") throw new ToHandleError();
            return res;
        }).then(res => {
            if(res.status.toString()[0] !== "2") throw new StatusCodeError(res);
            return options.isParseJson === false ? [] : res.json();
        }, (e: Error) => {

            if(e instanceof ToHandleError) throw e;
            console.log("Ошибка запроса к серверу. Метод: " + data.method,
                `Запрос: ${JSON.stringify(data)}`, e.message, e.stack || '');
            throw new HandledError();

        }).then(resolver, (errParsingJson: Error) => {

            if(errParsingJson instanceof ToHandleError) throw errParsingJson;
            if (errParsingJson instanceof HandledError) throw errParsingJson;
            if(errParsingJson instanceof StatusCodeError){
                errParsingJson.response.json().then(ans=>{
                    this.rootStore.setSnackBar(ans.message, true);
                })
                throw new ToHandleError();
            }
            console.log("Ошибка получения JSON с сервера. Метод " + data.method,
                errParsingJson.message,
                errParsingJson.stack || '',
                `Данные: ${JSON.stringify(data)}`,
            );
            alert('errParsingJson' + JSON.stringify(errParsingJson));

        }).then(null, (resolverErr: Error) => {

            if (resolverErr instanceof ToHandleError && isThrowError) throw resolverErr;
            if (resolverErr instanceof HandledError) return;
            console.log("Ошибка в обработчике ответа от сервера. Метод " + data.method,
                `Сообщение: ${resolverErr.message}`,
                `Стак ошибки: ${resolverErr.stack}`
            );

        });
    };
}
class HandledError extends Error {}
class ToHandleError extends Error {}
class StatusCodeError extends Error {
    constructor(res: Response) {
        super(res.status.toString());
        this.response = res;
    }
    response: Response;
}