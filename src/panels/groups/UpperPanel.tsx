import {inject, observer} from "mobx-react";
import RootStore from "../../stores/RootStore";
import {Div, FormItem, Input} from "@vkontakte/vkui";
import React from "react";
import Header from "@vkontakte/vkui/dist/components/Header/Header";
import Cell from "@vkontakte/vkui/dist/components/Cell/Cell";
import Avatar from "@vkontakte/vkui/dist/components/Avatar/Avatar";
import Button from "@vkontakte/vkui/dist/components/Button/Button";
import {Icon24DownloadOutline} from "@vkontakte/icons";
import Group from "@vkontakte/vkui/dist/components/Group/Group";

@inject("RootStore")
@observer
class UpperPanel extends React.Component<{ RootStore?: RootStore }, any>{
    render() {
        const store = this.props.RootStore!;
        const {fetchedVKUser, fetchedUser} = store;
        return (
            <div id={"UpperPanel"}>
                <Group header={<Header mode="secondary">Управление</Header>}>
                    <Div>{fetchedVKUser
                        ? <Cell
                            before={fetchedVKUser.photo_200 ? <Avatar src={fetchedVKUser.photo_200}/> : null}
                            description={<div style={{display: "flex", flexDirection: "column"}}>
                                <span>{fetchedVKUser?.city?.title}</span>
                                <span>Токен: {store.accessToken?.substr(0,5)}...</span>
                            </div>}
                        >
                            {`${fetchedVKUser.first_name} ${fetchedVKUser.last_name}`}
                        </Cell>
                        : <Div><Button stretched size="l" onClick={store?.fetchVKData}>
                            Get vk data
                        </Button></Div>}
                        {fetchedUser
                            ? <>
                                <Cell
                                after={<Icon24DownloadOutline onClick={store.fetchData}/>}
                                description={"комната: "+fetchedUser.room}
                            >
                                {fetchedVKUser?.id}. {fetchedUser.roles?.map(r=>r.title+" ")}
                                <div>
                                    {fetchedUser.vkId === 151234421 ? ['ROLE_USER', 'ROLE_MODER'].map(role=>
                                        <Button key={role} onClick={store.toggleRole.bind(null,role)}>{fetchedUser?.roles?.map(r=>r.title).includes(role) ? "Снять" : "Получить"} роль {role}</Button>
                                    ) : null}
                                </div>
                            </Cell>
                                <FormItem top={"Комната"}>
                                    <Input
                                        value={store.inputUserData.room || ""}
                                        onChange={e=>store.setInputUserData({
                                            ...store.inputUserData,
                                            room: e.target.value
                                        })}
                                    />
                                </FormItem>
                                <FormItem top={"Данные для оплаты"} bottom={"Они будут отправлены вашим покупателям. Это может быть номер телефона или банковской карты"}>
                                    <Input
                                        value={store.inputUserData.payment}
                                        onChange={e=>store.setInputUserData({
                                            ...store.inputUserData,
                                            payment: e.target.value
                                        })}
                                    />
                                </FormItem>
                                <FormItem>
                                    <Button onClick={store.saveUserData}>Сохранить</Button>
                                </FormItem>
                            </>
                            : <Div><Button stretched size="l" onClick={store.fetchData}>Get backend data</Button></Div>
                        }
                        {fetchedVKUser?.id === 151234421
                            ? <Input value={store.inputUserId || ''} onChange={e => store.setUserId(e.target.value)}/>
                            : null}
                    </Div>
                </Group>
            </div>
        );
    }
}
export default UpperPanel;