import RootStore from "./RootStore";
import {makeAutoObservable} from "mobx";
import Service from "./models/Service";
import User from "./models/User";
import ServiceRequest from "./models/ServiceRequest";

export default class ServicesStore{
    constructor(rootStore: RootStore) {
        makeAutoObservable(this);
        this.rootStore = rootStore;
    }

    rootStore: RootStore;

    isDev: boolean = process.env.NODE_ENV === "development";

    addingServiceData: addingService = {
        title: this.isDev ? "title lol" : "",
        price: this.isDev ? "1000" : "",
        description: this.isDev ? "desc desc desc" : "",
    };

    search: string = "";
    isSearchFocused: boolean = false;
    isSearching: boolean = false;

    popularServices: Service[] = [];
    myServices: Service[] = [];
    foundServices: Service[] = [];

    selectedTab: string = "services";

    myIncome?: number;

    changeTab = (val: string) => {
        this.selectedTab = val;
        this.rootStore.isHeaderOpened = false
    };

    onServicesInit = () => {
        return this.fetchServices()
            .then(this.fetchRequests)
            .then(this.fetchAnalytics);
    }

    setSearch = (val: string) => {
        this.search = val;
        if(val) {
            this.isSearching = true;
            this.fetchFoundServices(val);
        }
    };

    get isSearchActive(){
        return this.search.trim() !== ""
    }

    setIsSearchFocused = (val : boolean) => {
        this.isSearchFocused = val;
    }

    fetchServices = () => {
        return this.fetchPopularServices()
            .then(this.fetchMyServices);
    }

    fetchPopularServices = () => {
        return this.rootStore.doAjax("/services/popular", {}).then((res: service[]) => {
            this.popularServices = res.map(s=>{
                const owner = this.rootStore.getUserById(s.owner_id);
                return new Service(s, owner!); //TODO костыль
            });
        })
    };

    fetchMyServices = () => {
        return this.rootStore.doAjax("/services/my", {}).then((res: service[]) => {
            this.myServices = res.map(s=>{
                const owner = this.rootStore.getUserById(s.owner_id);
                return new Service(s, owner!); //TODO костыль
            });
        });
    }

    setAddingService = (val: addingService) => {
        this.addingServiceData = val;
    };

    addService = () => {
        return this.rootStore.doAjax("/services/", {}, {
            method: "POST",
            body: JSON.stringify(this.addingServiceData)
        }, this.fetchServices)
    };

    fetchFoundServices = (search: string) => {
        return this.rootStore.doAjax("/services/find/"+search, {}).then((res: service[]) => {
            this.foundServices = res.map(s=>{
                const owner = this.rootStore.getUserById(s.owner_id);
                return new Service(s, owner!); //TODO костыль
            });
        }).finally(()=>{
            this.isSearching = false;
        });
    }

    mapRequestsToObjects = (reqs: serviceRequest[]) => {
        return reqs.map(r=> new ServiceRequest(r, this.rootStore.getUserById(r.owner_id)!))
    }

    createRequest = (servicesIds: string[]) => this.rootStore.doAjax("/serviceRequests/", {}, {
        method: "POST",
        body: JSON.stringify(servicesIds)
    }, ()=>{
        this.rootStore.setSnackBar("Заказ успешно создан")
        this.fetchRequests()
    });

    hideService = (service: Service) => this.rootStore.doAjax("/services/" + service.id + "/hide", {}, {
        method: "PUT",
        isParseJson: false
    }, this.fetchServices);

    showService = (service: Service) => this.rootStore.doAjax("/services/" + service.id + "/open", {}, {
        method: "PUT",
        isParseJson: false
    }, this.fetchServices);

    myRequests: ServiceRequest[] = [];
    requestsToMe: ServiceRequest[] = [];

    fetchMyRequests = () => this.rootStore.doAjax("/serviceRequests/my", {}).then(res => {
        this.myRequests = this.mapRequestsToObjects(res);
    });

    fetchRequestsToMe = () => this.rootStore.doAjax("/serviceRequests/toMe", {}).then(res => {
        this.requestsToMe = this.mapRequestsToObjects(res);
    });

    fetchRequests = () => this.fetchMyRequests().then(this.fetchRequestsToMe);

    cancelRequest = (r: ServiceRequest) =>
        this.rootStore.doAjax("/serviceRequests/" + r.id + "/cancel", {}, {
            method: "DELETE",
            isParseJson: false
        }, this.fetchRequests)

    acceptRequest = (r: ServiceRequest) =>
        this.rootStore.doAjax("/serviceRequests/" + r.id + "/accept", {}, {
            method: "PUT",
            isParseJson: false,
        }, this.fetchRequests, true).catch(this.fetchRequests)

    fetchAnalytics = () => this.rootStore.doAjax("/services/income", {}, {}, res => {
        this.myIncome = res.income;
        console.log(res.income);
    });
}