import {makeAutoObservable} from "mobx";

export default class User implements user{
    constructor(user: user) {
        makeAutoObservable(this);
        this.roles = user.roles;
        this.room = user.room;
        this.vkData = user.vkData;
        this.vkId = user.vkId;
        this.paymentData = user.paymentData;
    }

    roles: { title: string }[];
    room: number | null;
    vkData?: { name: string; lastName: string; photo_200: string };
    vkId: number;
    paymentData: string;
}