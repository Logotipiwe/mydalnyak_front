import {makeAutoObservable} from "mobx";
import RootStore from "./RootStore";
import Delivery from "./models/Delivery";
import DeliveryOrder from "./models/DeliveryOrder";

class DeliveryStore implements storeWithTabs {
    constructor(store: RootStore) {
        makeAutoObservable(this);
        this.rootStore = store
    }

    rootStore: RootStore;

    changeTab(val: string): void {
        this.selectedTab = val;
    }

    selectedTab: string = "deliveries";

    isDev: boolean = process.env.NODE_ENV === "development";

    newDeliveryData: newDeliveryData = {
        place: this.isDev ? "5ка" : "",
        time: this.isDev ? "22:00" : "",
        stopAcceptOrdersTime: this.isDev ? "22:30" : "",
        markup: this.isDev ? "10" : ""
    }

    deliveries: Delivery[] = [];

    orders: DeliveryOrder[] = [];

    selectedDeliveryId?: string;

    deliveryRoute: string = "/delivery";

    deliveryOrdersRoute: string = "/deliveryDetails"

    addingDeliveryOrder: addingDeliveryOrder = {
        title: "",
        description: "",
        amount: "1"
    }

    buyingOrderId?: string;

    buyingOrderInputPrice: string = "";

    setBuyingOrderInputPrice(val: string) {
        this.buyingOrderInputPrice = val;
    }

    get buyingOrder() {
        return this.orders.find(o => o.id === this.buyingOrderId);
    }

    get deliveriesToShow() {
        return this.deliveries.slice().sort((a, b) => {
            return a.createdDate > b.createdDate ? 1 : -1;
        })
    }

    onDeliveriesInit = () => {
        return this.fetchDeliveries();
    }

    setAddingDeliveryOrder = (val: addingDeliveryOrder) => {
        this.addingDeliveryOrder = val;
    };

    fetchDeliveries = () => {
        return this.rootStore.doAjax(this.deliveryRoute + "/", {}).then((res: delivery[]) => {
            this.orders = []; //отчистка листа перед заполнением
            this.deliveries = res.map(d => {
                const orders = d.deliveryOrderList.map(o =>
                    new DeliveryOrder(o, this.rootStore.getUserById(o.owner_id)!))
                const delivery = new Delivery(d, this.rootStore.getUserById(d.owner_id)!, orders);
                orders.forEach(o => {
                    o.setDelivery(delivery);
                    this.orders.push(...orders);
                });
                return delivery;
            })
        })
    };

    setNewDeliveryData = (val: newDeliveryData) => {
        this.newDeliveryData = val;
        console.log(val);
    };

    get deliveryDataToPost() {
        const today = new Date();
        const timeToISO = (time: string) => {
            return [today.getFullYear(),
                (today.getMonth() + 1).toString().padStart(2, '0'),
                today.getDate().toString().padStart(2, '0')].join("-") + "T" + time + ":00"
        }
        const data = this.newDeliveryData;
        return {
            ...data,
            title: data.place,
            time: timeToISO(data.time),
            stopAcceptOrdersTime: timeToISO(data.stopAcceptOrdersTime),
            markUp: ((parseInt(data.markup) || 0) + 100) / 100
        }
    }

    getDeliveryById = (id?: string) => {
        if (!id) return undefined;
        return this.deliveries.find(d => d.id === id);
    }

    newDelivery = () => {
        if (this.newDeliveryData.time === "" || this.newDeliveryData.stopAcceptOrdersTime === "") {
            this.rootStore.setSnackBar("Введите время", true);
            return;
        }
        return this.rootStore.doAjax(this.deliveryRoute + "/", {}, {
            method: "POST",
            body: JSON.stringify(this.deliveryDataToPost)
        }, res => {
            this.fetchDeliveries()
        })
    };

    timeFromLocaleDate(date: string) {
        const timeArr = date.split("T")[1].split(":");
        return timeArr[0] + ":" + timeArr[1];
    }

    onModalClose = () => {
        this.selectedDeliveryId = undefined;
    }

    selectDelivery = (d: Delivery) => {
        this.selectedDeliveryId = d.id;
        this.fetchDeliveries();
    };

    addDeliveryOrder = (d: Delivery) => {
        if (!this.addingDeliveryOrder.title || !this.addingDeliveryOrder.amount) {
            this.rootStore.setSnackBar("Введите название и количество");
            return;
        }
        return this.rootStore.doAjax(this.deliveryOrdersRoute + "/" + d.id + "/add", {}, {
            method: 'POST',
            body: JSON.stringify(this.addingDeliveryOrder)
        }, res => {
            this.fetchDeliveries()
        })
    };

    cancelDelivery = (d: Delivery) => {
        return this.rootStore.doAjax(this.deliveryRoute + "/" + d.id + "/cancel", {}, {
            method: "PUT"
        }, res => {
            this.fetchDeliveries()
        })
    }

    boughtDelivery = (d: Delivery) => {
        if(!d.deliveryOrderList.every(o=>o.isBought)){
            this.rootStore.setSnackBar("Остались необработанные заказы", true);
            return;
        }
        return this.rootStore.doAjax(this.deliveryRoute + "/" + d.id + "/bought", {}, {
            method: "PUT"
        }, res => {
            this.fetchDeliveries()
        })
    };

    finishDelivery = (d: Delivery) => {
        return this.rootStore.doAjax(this.deliveryRoute + "/" + d.id + "/delivered", {}, {
            method: "PUT"
        }, res => {
            this.fetchDeliveries();
        })
    };

    rejectOrder = (o: DeliveryOrder) => {
        return this.rootStore.doAjax(this.deliveryOrdersRoute + "/" + o.id + "/reject", {}, {
            method: "PUT"
        }, res => {
            this.fetchDeliveries()
        })
    };

    acceptOrder = (o: DeliveryOrder) => this.rootStore.doAjax(this.deliveryOrdersRoute + "/" + o.id + "/accept", {}, {
        method: "PUT"
    }, res => {
        this.fetchDeliveries()
    });

    buyOrder = (o?: DeliveryOrder) => {
        if (!o || !this.buyingOrderInputPrice) {
            this.rootStore.setSnackBar("Введите цену", true);
            return;
        }
        return this.rootStore.doAjax(
            this.deliveryOrdersRoute + "/" + o.id + "/bought/" + this.buyingOrderInputPrice,
            {},
            {
                method: "PUT"
            }, res => {
                this.buyingOrderId = undefined;
                this.fetchDeliveries()
            })
    };

    openBuyOrderModal = (o: DeliveryOrder) => {
        this.buyingOrderId = o.id;
    };
}

export default DeliveryStore;