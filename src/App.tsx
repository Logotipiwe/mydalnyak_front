import React, { useState, useEffect } from 'react';
import bridge from '@vkontakte/vk-bridge';
import View from '@vkontakte/vkui/dist/components/View/View';
import {AdaptivityProvider, AppRoot, Panel, ScreenSpinner} from '@vkontakte/vkui';
import '@vkontakte/vkui/dist/vkui.css';

import Home from './panels/Home';
import {inject, observer} from "mobx-react";
import RootStore from "./stores/RootStore";

@inject("RootStore")
@observer
class App extends React.Component<{RootStore?: RootStore}, any> {
	componentDidMount() {
		const store = this.props.RootStore!;

		store.onAppInit();

		bridge.subscribe((res) => {
			if (res.detail.type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				schemeAttribute.value = res.detail.data.scheme ? res.detail.data.scheme : 'client_light';
				// schemeAttribute.value = 'space_gray';
				document.body.attributes.setNamedItem(schemeAttribute);
				console.log('new theme ' + schemeAttribute.name, schemeAttribute.value);
			}
		});
	}

	render() {
		return (
			<AdaptivityProvider>
				<AppRoot>
					<View activePanel={'home'}>
						<Panel id={'home'}>
							<Home/>
						</Panel>
					</View>
				</AppRoot>
			</AdaptivityProvider>
		);
	}
}

export default App;

