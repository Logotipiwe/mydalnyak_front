import React from "react";
import ReactDOM from "react-dom";
import bridge from "@vkontakte/vk-bridge";
import App from "./App";
import {Provider} from "mobx-react";
import RootStore from "./stores/RootStore";

const rootStore: RootStore = new RootStore();

ReactDOM.render(
    <Provider RootStore={rootStore}>
        <App/>
    </Provider>,
    document.getElementById("root"));
/*if (process.env.NODE_ENV === "development") {
    import("./eruda").then(({default: eruda}) => {
    }); //runtime download
}*/
