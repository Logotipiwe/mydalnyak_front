import Ownable from "./Ownable";
import User from "./User";

export default class Service extends Ownable implements service{
    constructor(service: service, owner: User) {
        super(service, owner);

        this.price = service.price;
        this.status = service.status
    }

    price: number;
    status: ServiceStatus;

    get isHidden(){
        return this.status === "HIDDEN"
    }
}