import Ownable from "./Ownable";
import User from "./User";
import DeliveryOrder from "./DeliveryOrder";
import {makeObservable, observable} from "mobx";

export default class Delivery extends Ownable implements delivery{
    constructor(delivery: delivery, owner: User, orders: DeliveryOrder[]) {
        super(delivery, owner);
        makeObservable(this, {
            deliveryOrderList: observable
        })

        this.time = delivery.time;
        this.deliveryStatus = delivery.deliveryStatus;
        this.stopAcceptOrdersTime = delivery.stopAcceptOrdersTime;
        this.place = delivery.place;

        this.deliveryOrderList = orders;
    }

    time: string;
    deliveryStatus: deliveryStatus;
    stopAcceptOrdersTime: string;
    place: string;

    deliveryOrderList: DeliveryOrder[]

    price?: number;

    ordersByUser = (vkId: number) => this.deliveryOrderList
        .slice().sort((a, b) => {
            return a.createdDate > b.createdDate ? 1 : -1;
        })
        .filter(o => o.owner_id === vkId);
}