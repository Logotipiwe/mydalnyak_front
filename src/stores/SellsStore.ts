import RootStore from "./RootStore";
import Product from "./models/Product";
import bridge from "@vkontakte/vk-bridge";
import {ChangeEvent} from "react";
import {makeAutoObservable, makeObservable} from "mobx";

export default class SellsStore {
    constructor(rootStore: RootStore) {
        makeAutoObservable(this);

        this.rootStore = rootStore;
    }

    rootStore: RootStore;

    uploadingPhoto?: File;

    categories?: category[];

    cart: product[] = [];

    products?: Product[];

    orders?: order[];

    newProdSubs?: newProdSub[];

    newProdData: newProdData = {
        title: '',
        price: ''
    }

    ordersToMe?: order[];

    myNewAdmissionSubs?: admissionSub[];

    selectedTab: string = "products";

    productsSearch: string = "";

    productAmountInputId?: string;

    prodAmountInput?: number;

    setProdAmountInput(val?: number){
        this.prodAmountInput = val;
    }

    setProductAmountInputId = (val?: string) => {
        this.productAmountInputId = val;
    };

    onProdAmountModalClose = () => {
        this.productAmountInputId = undefined;
        this.prodAmountInput = undefined;
    };

    onSellsInit = () => {
        this.fetchProdsByCat()
            .then(this.fetchOrders)
            .then(this.fetchNewProdSubs)
    }

    setProdsSearch = (val: string) => {
        this.productsSearch = val;
    };

    get productsToShow(){
        if(!this.products) return [];
        return this.products.slice()
            .sort((a, b) => (a.createdDate > b.createdDate ? 1 : -1))
            .filter(p=>p.title.toLowerCase().includes(this.productsSearch.toLowerCase()));
    }

    get myProducts(){
        if(!this.products) return [];
        return this.products.filter(p=>p.owner.vkId === this.rootStore.fetchedUser?.vkId)
    }

    changeTab = (val: string) => {
        this.selectedTab = val;
        this.rootStore.isHeaderOpened = false;
    };

    clearCats = () => {
        this.categories = undefined;
    };

    fetchCategories = () => {
        this.rootStore.doAjax("/product_categories/").then((res: category[]) => {
            this.categories = res;
        })
    };

    getProd = (id: string) => this.products?.find(p => p.id === id);

    clearProds = () => {
        this.products = undefined;
    };

    fetchProdsByCat = (cat?: category) => {
        const route = cat ? ("/product_categories/" + cat.id + "/products") : "/products";

        return this.rootStore.doAjax(route).then((res: product[]) => {
            this.products = res.map(productRes => {
                const user = this.rootStore.getUserById(productRes.owner_id);
                const product = new Product(productRes, user!); //TODO костыль

                this.rootStore.fetchRating(product).then((res: rating) => {
                    product.rating = res;
                }, e => {/*if rating not found*/
                })

                return product;
            });
        }).then(this.getAdmissionSubs)
    };

    fetchOrders = () => this.rootStore.doAjax("/orders/findByOwner", {
        ownerId: this.rootStore.urlKeys.get("vk_user_id")
    }).then((res: order[]) => {
        this.orders = res;
    }).then(() => this.rootStore.doAjax("/orders/findBySeller").then((res: order[]) => {
        this.ordersToMe = res;
    }));

    addCart = (prod: product) => {
        this.cart.push(prod);
        this.rootStore.setSnackBar("Товар добавлен в корзину")
    };

    delCart = (id: string) => {
        const found = this.cart.find(p => p.id === id);
        if (found) this.cart.splice(
            this.cart.indexOf(found), 1
        );
    };

    get cartFormatted() {
        return this.cart.reduce<cartItem[]>(
            (arr, prod) => {
                const found = arr.find(x => x["id"] === prod.id);
                if (found) found.amount += 1;
                else arr.push({id: prod.id, amount: 1});
                return arr;
            }, []
        )
    }

    newOrder = () => {
        const prodIds = this.cart.map(p => p.id);
        this.rootStore.doAjax("/orders/", {}, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(prodIds)
        }, (res: crnkAns<order[]>) => {
            this.cart = [];
            this.fetchOrders().finally(() => this.fetchProdsByCat());
        }).catch(() => {
        })
    };


    fetchNewProdSubs = () => {
        return this.rootStore.doAjax("/subscriptions/new_prod/my").then((res: newProdSub[]) => {
            this.newProdSubs = res;
        })
    };

    subscribeToNewProd = (seller_id: number) => {
        this.rootStore.doAjax("/subscriptions/new_prod/", {}, {
            method: "POST",
            headers: {},
            body: JSON.stringify({sellerId: seller_id})
        }, this.fetchNewProdSubs).catch(() => {})
    };

    unsubFromNewProd = (id: string) => {
        this.rootStore.doAjax("/subscriptions/new_prod/" + id, {}, {
            method: "DELETE",
            isParseJson: false
        }, this.fetchNewProdSubs).catch(() => {
        });
    };

    setProdData = (val: newProdData) => {
        this.newProdData = val;
    }

    newProduct = () => {
        if(!this.newProdData.title || !parseInt(this.newProdData.price)){
            this.rootStore.setSnackBar("Введите цену и название", true);
            return;
        }
        return this.rootStore.uploadFile(this.uploadingPhoto)
            .then((url: string|null) => {
                return this.rootStore.doAjax("/products/", {}, {
                    method: "POST",
                    body: JSON.stringify({...this.newProdData, photo: url})
                }, res => {
                })
            }, e => {
                this.rootStore.setSnackBar("Не удалось загрузить фото товара", true);
            })
            .then(() => {
                this.rootStore.setSnackBar("Товар добавлен");
                return this.fetchProdsByCat();
            })
    };

    delProduct = (p: product, isFetch: boolean = true) => {
        return this.rootStore.doAjax("/products/" + p.id + "/hide", {}, {
            method: "PUT",
            isParseJson: false,
        }, () => {
            this.rootStore.setSnackBar("Товар удален")
            if (isFetch) this.fetchProdsByCat();
        }).catch(() => {
        })
    }


    updateOrder = (order: order, action: "accept" | "cancel") => {
        this.rootStore.doAjax("/orders/" + action + "/" + order.id, {}, {
            method: "PUT"
        }, res => {
            return this.fetchOrders();
        }).catch(() => {
        })
    }

    setProductAmount = (product: product, amount: number) => this.rootStore.doAjax("/products/" + product.id + "/stock/" + amount, {}, {
        method: "PUT",
        isParseJson: false,
    }, () => {
        this.rootStore.setSnackBar("Количество товара увеличено")
        return this.fetchProdsByCat();
    }).finally(()=>{
        this.onProdAmountModalClose();
    });

    getAdmissionSubByProd = (prod: product) => this.myNewAdmissionSubs?.find(sub => sub.product.id === prod.id);

    getAdmissionSubs = () => {
        return this.rootStore.doAjax("/subscriptions/newAdmission/my").then((res: admissionSub[]) => {
            this.myNewAdmissionSubs = res;
        })
    };

    subscribeToAdmission = (prod: product) => {
        return this.rootStore.doAjax("/subscriptions/newAdmission", {}, {
            method: "POST",
            body: JSON.stringify({productId: prod.id})
        }, () => {
            this.fetchProdsByCat();
        }).catch(() => {
        })
    }

    unsubscribeFromAdmission = (id: string) => {
        return this.rootStore.doAjax("/subscriptions/newAdmission/" + id, {}, {
            method: "DELETE",
            isParseJson: false
        }, () => {
            return this.fetchProdsByCat();
        }).catch(() => {
        })
    }

    setUploadingPhoto = (val: ChangeEvent<HTMLInputElement>) => {
        if (!val.target.files) return;
        this.uploadingPhoto = val.target.files[0];
    };
}