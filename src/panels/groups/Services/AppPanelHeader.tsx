import {Cell, Counter, List, PanelHeader, PanelHeaderContext, Tabs, TabsItem} from "@vkontakte/vkui";
import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import React, {ReactNode} from "react";
import {Icon16Dropdown, Icon20ServicesOutline} from "@vkontakte/icons";

/*
* [
            rootStore.getTabObj("services", "Услуги", store.fetchServices),
            rootStore.getTabObj("requests", "Заявки", store.fetchRequests, store.requestsToMe.filter(r=>r.status === "OPENED").length),
            rootStore.getTabObj("analytics", "Аналитика", store.fetchAnalytics)
        ]
        * <Icon20ServicesOutline style={{color: "var(--accent)"}}/>
        * */


@inject("RootStore")
@observer
class AppPanelHeader extends React.Component<{ RootStore?: RootStore, tabs: tabObj[], store: storeWithTabs, icon: ReactNode }, any>{
    render() {
        const rootStore = this.props.RootStore!;
        const store = this.props.store;
        const tabs = this.props.tabs
        const currTab = tabs.find(t=>t.key === store.selectedTab)!;
        return (<>
                <PanelHeader>
                    <Tabs>
                        {this.props.icon}
                        <TabsItem
                            onClick={rootStore.setIsHeaderOpened.bind(null, !rootStore.isHeaderOpened)}
                            after={<Icon16Dropdown style={{
                                transform: `rotate(${rootStore.isHeaderOpened ? '180deg' : '0'}) translateY(1px)`
                            }}/>}
                        >{currTab.title}</TabsItem>
                    </Tabs>
                </PanelHeader>
                <PanelHeaderContext opened={rootStore.isHeaderOpened} onClose={()=>rootStore.setIsHeaderOpened(false)}>
                    <List>
                        {tabs.map(tab=>
                            <Cell
                                key={tab.key}
                                onClick={()=>{
                                    store.changeTab(tab.key)
                                    tab.onSelect()
                                }}
                                after={tab.counter ? <Counter mode={"prominent"}>{tab.counter}</Counter> : null}
                            ><span style={{color: store.selectedTab === tab.key ? "var(--accent)": undefined}}>{tab.title}</span></Cell>
                        )}
                    </List>
                </PanelHeaderContext>
            </>
        );
    }
}
export default AppPanelHeader;