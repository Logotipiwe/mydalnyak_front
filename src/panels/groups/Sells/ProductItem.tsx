import React from "react";
import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import {ContentCard} from "@vkontakte/vkui";
import Button from "@vkontakte/vkui/dist/components/Button/Button";
import {
    Icon16Cancel,
    Icon16StarCircle,
    Icon16StarCircleFillYellow,
    Icon20NotificationOutline,
    Icon24CubeBoxOutline,
    Icon24NotificationSlashOutline,
    Icon24ShoppingCartOutline
} from "@vkontakte/icons";
import Product from "../../../stores/models/Product";

@inject("RootStore")
@observer
class ProductItem extends React.Component<{ RootStore?: RootStore, product: Product }, any> {
    render() {
        const product = this.props.product;
        const rootStore = this.props.RootStore!;
        const store = rootStore.sellsStore;
        const seller = rootStore.users?.find(u => u.vkId === product.owner_id);
        const subscription = store.getAdmissionSubByProd(product);
        const productRating = product.rating;
        const image = product.photo ? (rootStore.url + "/files/" + product.photo) : product.owner?.vkData?.photo_200;
        return <ContentCard
            mode={"outline"}
            key={product.id}
            image={image}
            header={product.title}
            text={<>{product.price} p. ({product.stockAmount} шт.)</>}
            maxHeight={200}
            caption={<>
                <div><span>{seller?.vkData?.name} {seller?.vkData?.lastName}</span></div>
                {productRating
                    ? <>
                        <div><span>{productRating.ratingsCount > 0
                            ? <>Оценка: {((productRating.rating || 0) / 20).toFixed(1)}</>
                            : <>Оценок нет</>
                        }</span></div>
                        <div style={{display: "flex"}}>
                            {new Array(5).fill('').map((x, i) => {
                                const style = {margin: "5px", cursor: "pointer"};
                                const myRating: number | undefined = product.rating?.myRating;
                                const onClick = rootStore.setRating.bind(null, product, (i + 1) * 20);
                                if (myRating && (myRating / 20 >= i + 1)) {
                                    return <Icon16StarCircleFillYellow key={i} style={style} onClick={onClick}/>;
                                }
                                return <Icon16StarCircle key={i} style={style} onClick={onClick}/>;
                            })}
                        </div>
                    </> : null}
                <div style={{display: "flex"}}>
                    {subscription
                        ? <Button
                            size={"s"}
                            onClick={() => store.unsubscribeFromAdmission(subscription.id)}
                        ><Icon24NotificationSlashOutline/></Button>
                        : <Button
                            size={"s"}
                            onClick={() => store.subscribeToAdmission(product)}>
                            <Icon20NotificationOutline/></Button>
                    }
                    <Button
                        size={"s"}
                        mode={"commerce"}
                        onClick={store.addCart.bind(null, product)}
                    ><Icon24ShoppingCartOutline/></Button>
                    <Button
                        size={"s"}
                        mode={"secondary"}
                        onClick={() => store.setProductAmountInputId(product.id)}
                    ><Icon24CubeBoxOutline/></Button>
                    <Button
                        size={"s"}
                        mode={"destructive"}
                        onClick={() => store.delProduct(product)}
                    ><Icon16Cancel/></Button>
                </div>
            </>}
        />
    }
}

export default ProductItem;