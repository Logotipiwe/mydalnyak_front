import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import React, {ReactNode} from "react";
import {Avatar, Card, Cell, File, FormItem, Group, Header, Input, Panel} from "@vkontakte/vkui";
import {Icon16Add, Icon24Camera} from "@vkontakte/icons";
import Button from "@vkontakte/vkui/dist/components/Button/Button";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import DeliveryItem from "./DeliveryItem";

@inject("RootStore")
@observer
class DeliveriesPanel extends React.Component<{ RootStore?: RootStore, id: string, header: ReactNode }, any> {
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.deliveryStore;

        return (
            <Panel id={"deliveries"}>
                {this.props.header}
                <Div>
                    <Card mode={"tint"}>
                        <Group header={<Header mode={"secondary"}>Новая доставка</Header>}>
                            <Div>
                                <FormItem top={"Магазин"}>
                                    <Input
                                        style={{display: "flex", flexGrow: 1}}
                                        value={store.newDeliveryData.place}
                                        onChange={e => store.setNewDeliveryData({
                                            ...store.newDeliveryData,
                                            place: e.target.value
                                        })}
                                    />
                                </FormItem>
                                <div style={{display: "flex"}}>
                                    <FormItem top={"Прием заявок до"}>
                                        <Input
                                            type={"time"}
                                            value={store.newDeliveryData.stopAcceptOrdersTime}
                                            onChange={e => store.setNewDeliveryData({
                                                ...store.newDeliveryData,
                                                stopAcceptOrdersTime: e.target.value
                                            })}
                                        />
                                    </FormItem>
                                    <FormItem top={"Время похода"}>
                                        <Input
                                            type={"time"}
                                            value={store.newDeliveryData.time}
                                            onChange={e => store.setNewDeliveryData({
                                                ...store.newDeliveryData,
                                                time: e.target.value
                                            })}
                                        />
                                    </FormItem>
                                    <FormItem top={"Наценка в %"}>
                                        <Input
                                            value={store.newDeliveryData.markup}
                                            onChange={e => store.setNewDeliveryData({
                                                ...store.newDeliveryData,
                                                markup: e.target.value
                                            })}
                                        />
                                    </FormItem>
                                </div>
                                <Div><Button mode={"commerce"} onClick={store.newDelivery}>Создать доставку</Button></Div>
                            </Div>
                        </Group>
                    </Card>
                </Div>
                <Div>
                    <Card mode={"tint"}>
                        <Group header={<Header mode={"secondary"}>Активные доставки</Header>}>
                            {store.deliveriesToShow.map(d=>
                                <DeliveryItem delivery={d} key={d.id}/>
                            )}
                        </Group>
                    </Card>
                </Div>
            </Panel>
        );
    }
}

export default DeliveriesPanel;