import {inject, observer} from "mobx-react";
import RootStore from "../../stores/RootStore";
import React from "react";
import {
    Button,
    FormItem,
    FormLayout, Input,
    ModalPage, ModalPageHeader,
    ModalRoot,
    View
} from "@vkontakte/vkui";
import ProductsPanel from "./Sells/ProductsPanel";
import OrdersPanel from "./Sells/OrdersPanel";
import CartPanel from "./Sells/CartPanel";
import SubscriptionsPanel from "./Sells/SubscriptionsPanel";
import AppPanelHeader from "./Services/AppPanelHeader";
import {Icon28MarketOutline} from "@vkontakte/icons";
import MyProductsPanel from "./Sells/MyProductsPanel";
import DeliveryModal from "./Delivery/DeliveryModal";

@inject("RootStore")
@observer
class Sells extends React.Component<{ RootStore?: RootStore, id: activeStories }, any> {
    componentDidMount() {
        this.props.RootStore!.sellsStore.onSellsInit();
    }

    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.sellsStore;

        const header = <AppPanelHeader tabs={[
            rootStore.getTabObj("products", "Товары", store.fetchProdsByCat),
            rootStore.getTabObj("orders", "Заказы", store.fetchOrders, store.ordersToMe?.length),
            rootStore.getTabObj("cart", "Корзина", ()=>{}, store.cart.length),
            rootStore.getTabObj("subscriptions", "Подписки", store.fetchNewProdSubs),
            rootStore.getTabObj("myProds", "Мои товары", store.fetchProdsByCat)
        ]
        } store={store} icon={<Icon28MarketOutline/>}/>

        let modal;
        if(store.productAmountInputId) {
            const settingAmountProduct = store.getProd(store.productAmountInputId);
            if(!settingAmountProduct) return;
            modal = <ModalRoot activeModal={"setAmount"} onClose={store.onProdAmountModalClose}>
                <ModalPage
                    id={"setAmount"}
                    header={<ModalPageHeader>Добавить количество: {settingAmountProduct.title}</ModalPageHeader>}
                >
                    <FormLayout>
                        <FormItem top={"Количество"} bottom={"Это количество товара будет добавлено"}>
                            <Input value={store.prodAmountInput}
                                   onChange={e=>store.setProdAmountInput(parseInt(e.target.value) || undefined)}
                            />
                        </FormItem>
                        <FormItem>
                            <Button
                                mode={"commerce"}
                                stretched
                                onClick={(store.prodAmountInput && parseInt(store.prodAmountInput.toString()) > 0)
                                    ? ()=>store.setProductAmount(settingAmountProduct, settingAmountProduct.stockAmount + (store.prodAmountInput || 0))
                                    : undefined
                                }
                            >Добавить</Button>
                        </FormItem>
                    </FormLayout>
                </ModalPage>
            </ModalRoot>;
        }

        return (
            <View id={'sells'} activePanel={store.selectedTab} modal={modal}>
                <ProductsPanel id={"products"} header={header}/>
                <CartPanel id={"cart"} header={header}/>
                <OrdersPanel id={"orders"} header={header}/>
                <SubscriptionsPanel id={'subscriptions'} header={header}/>
                <MyProductsPanel id={"myProds"} header={header}/>
            </View>
        );
    }
}

export default Sells;