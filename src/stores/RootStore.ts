import {makeAutoObservable} from "mobx";
import bridge, {UserInfo} from "@vkontakte/vk-bridge";
import NetworkStore from "./NetworkStore";
import {ChangeEvent, ReactNode} from "react";
import User from "./models/User";
import Product from "./models/Product";
import Ownable from "./models/Ownable";
import SellsStore from "./SellsStore";
import ServicesStore from "./ServicesStore";
import DeliveryStore from "./DeliveryStore";

export default class RootStore {
    constructor() {
        makeAutoObservable(this);

        this.fetchVKData = this.fetchVKData.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.getUsers = this.getUsers.bind(this);


        this.networkStore = new NetworkStore(this);

        this.sellsStore = new SellsStore(this)
        this.servicesStore = new ServicesStore(this);
        this.deliveryStore = new DeliveryStore(this);
    }

    networkStore: NetworkStore;

    sellsStore: SellsStore;
    servicesStore: ServicesStore;
    deliveryStore: DeliveryStore;

    url: string = process.env.NODE_ENV === "development"
        ? "http://localhost:8080"
        // ? "https://27e7c2888a10.eu.ngrok.io"
        : "https://logotipiwe.ru/spring";

    isLoading: boolean = false;

    fetchedVKUser: UserInfo | null = null;

    fetchedUser: user | null = null;

    accessToken?: string;

    v: string = "5.130"

    urlKeys = new URLSearchParams(window.location.search);

    inputUserId: string | null = this.urlKeys.get("vk_user_id");

    snackbar?: { text: ReactNode, isRed: boolean };

    isInited: boolean = false;

    isDev: boolean = process.env.NODE_ENV === "development";

    isHeaderOpened: boolean = false;

    inputUserData = {
        room: "",
        payment: ""
    }

    setInputUserData = (val: {room: string, payment: string}) => {
        this.inputUserData = val;
    };

    saveUserData = () => {
        return this.doAjax("/users/setRoom/"+(this.inputUserData.room || "null"), {}, {
            method: "POST",
            isParseJson: false
        }, () => {
            return this.doAjax("/users/setPaymentData/"+this.inputUserData.payment, {}, {
                method: "POST",
                isParseJson: false
            }, this.fetchData)
        });
    };

    setIsHeaderOpened = (val: boolean) => {
        this.isHeaderOpened = val;
    };

    onHideSnackBar = () => {
        this.snackbar = undefined;
    };

    getTabObj(key: string, title: string, onSelect: ()=>any, counter?: number): tabObj{
        return {key, title, counter, onSelect};
    }

    activeStory: activeStories = this.isDev ? "delivery" : "sells";

    setActiveStory = (story: activeStories) => {
        this.activeStory = story;
    };

    setSnackBar = (text: ReactNode, isRed: boolean = false) => {
        if (this.snackbar) this.snackbar = undefined;
        this.snackbar = {text, isRed}
    }

    doAjax<T extends networkRes>(route: string): Promise<any>;
    doAjax<T extends networkRes>(route: string, data: networkObj): Promise<any>;
    doAjax<T extends networkRes>(route: string, data: networkObj, resolver: (res: any) => void): Promise<any>;
    doAjax<T extends networkRes>(route: string, data: networkObj, options: object, resolver: (res: any) => void): Promise<any>;
    doAjax<T extends networkRes>(route: string, data: networkObj, options: object, resolver: (res: any) => void, isThrowError: boolean): Promise<any>;

    doAjax<T extends networkRes>(
        route: string,
        data: networkObj = {},
        optionsOrResolver?: object | ((res: any) => void),
        resolver?: (res: any) => void,
        isThrowError: boolean = false
    ): Promise<any> {
        if (arguments.length === 1) {
            return this.networkStore.doAjax(route, data, {}, (x: any) => x, isThrowError);
        } else if (arguments.length === 2) {
            return this.networkStore.doAjax(route, data, {}, optionsOrResolver as (res: any) => void, isThrowError);
        } else if (arguments.length === 3) {
            return this.networkStore.doAjax(route, data, optionsOrResolver as object, resolver!, isThrowError);
        } else {
            return this.networkStore.doAjax(route, data, optionsOrResolver as object, resolver!, isThrowError);
        }
    }

    onAppInit = () => {
        return bridge.send("VKWebAppInit")
            .then(() => {
                return bridge.send("VKWebAppGetAuthToken", {
                    scope: "stats",
                    app_id: parseInt(this.urlKeys.get('vk_app_id') || '')
                }).then((res: any) => {
                    this.accessToken = res.access_token;
                    return this.fetchVKData()
                        .then(this.fetchData)
                        .then(this.getUsers)
                        .then(() => {
                            this.isInited = true;
                        })
                })
            })
    }

    setUserId = (val: string) => {
        this.inputUserId = val;
    }

    fetchVKData() {
        return bridge.send('VKWebAppGetUserInfo')
            .then(user => {
                this.fetchedVKUser = user;
                this.isLoading = false;
            })
    }

    fetchData() {
        return this.doAjax("/users/my_data").then((res: user) => {
            this.fetchedUser = res;
            this.inputUserData = {
                room: res.room ? res.room.toString() : "",
                payment: res.paymentData
            }
        });
    }

    fetchRating(ownable: Ownable) {
        return this.doAjax("/ownable/" + ownable.id + "/rating")
    }

    getUserById = (id: number) => {
        return this.users.find(u => u.vkId === id);
    };

    users: User[] = [];

    getUsers() {
        if (!this.accessToken) {
            this.setSnackBar("Ошибка получения access_token пользователя", true);
            return;
        }

        return this.doAjax("/users/").then((usersRes: user[]) => {
            return bridge.send("VKWebAppCallAPIMethod", {
                method: "users.get",
                request_id: Math.round(Math.random() * 1000),
                params: {
                    access_token: this.accessToken!,
                    v: this.v,
                    user_ids: usersRes.map(u => u.vkId).join(','),
                    fields: 'photo_200'
                }
            }).then((VkUsersRes) => {
                this.users = [];
                usersRes.forEach(user => {
                    const vk_user = VkUsersRes.response.find((u: any) => u.id === user.vkId);
                    user.vkData = {
                        name: vk_user.first_name,
                        lastName: vk_user.last_name,
                        photo_200: vk_user.photo_200
                    }

                    this.users.push(new User(user));
                })
            })
        })
    }

    uploadFile = (file?: File) => {
        console.log(file);
        if(!file) return new Promise(resolve => resolve(null));
        const formData = new FormData();
        formData.append("file", file);
        return this.doAjax("/files/", {}, {
            method: "POST",
            dontSetContentType: true,
            body: formData
        }, (res: { url: string }) => {
            return res.url;
        });
    };

    toggleRole = (role: string) => this.doAjax("/users/toggleRole/" + role, {}, {
        method: "POST",
        isParseJson: false
    }, res => {
        const roles = this.fetchedUser!.roles;
        let found = roles.find(r => r.title === role);
        const index = found ? roles.indexOf(found) : -1;
        if (index === -1) {
            roles.push({title: role})
        } else roles.splice(index, 1);
    });

    setRating = (ownable: Ownable, rating: number) => this.doAjax("/ownable/" + ownable.id + "/rating/" + rating, {}, {
        method: "POST"
    }, (res: rating) => {
        ownable.setRating(res);
    }).catch(() => {
    });
}