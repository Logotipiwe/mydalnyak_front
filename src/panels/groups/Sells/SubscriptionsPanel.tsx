import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import React, {ReactNode} from "react";
import {Avatar, Card, Cell, Panel} from "@vkontakte/vkui";
import Header from "@vkontakte/vkui/dist/components/Header/Header";
import Button from "@vkontakte/vkui/dist/components/Button/Button";
import {Icon16Add, Icon16DownloadOutline} from "@vkontakte/icons";
import SellsPanelHeader from "./SellsPanelHeader";

@inject("RootStore")
@observer
class SubscriptionsPanel extends React.Component<{ RootStore?: RootStore, id: string, header: ReactNode }, any>{
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.sellsStore;
        return (
            <Panel id={"subscriptions"}>
                {this.props.header}
                <Card mode={"tint"}>
                    <Header aside={<>
                        <Button onClick={rootStore.getUsers}><Icon16DownloadOutline/></Button>
                    </>
                    }>Продавцы</Header>
                    {rootStore.users?.map(u => {
                            const subscription = store.newProdSubs?.find(s => s.sellerId === u.vkId);
                            return <Cell
                                key={u.vkId}
                                before={<Avatar src={u.vkData?.photo_200}/>}
                                after={!store.newProdSubs
                                    ? null
                                    : <Button
                                        onClick={subscription
                                            ? store.unsubFromNewProd.bind(null, subscription.id)
                                            : store.subscribeToNewProd.bind(null, u.vkId)
                                        }>
                                        <div style={{display: "flex", alignItems: "center"}}>
                                            {subscription
                                                ? <>Отписаться</>
                                                : <>нов.товары <Icon16Add/></>
                                            }
                                        </div>
                                    </Button>
                                }
                            >{u.vkData?.name} {u.vkData?.lastName}</Cell>;
                        }
                    )}
                </Card>
            </Panel>
        );
    }
}
export default SubscriptionsPanel;