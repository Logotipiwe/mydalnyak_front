import Ownable from "./Ownable";
import User from "./User";

export default class ServiceRequest extends Ownable{
    constructor(req: serviceRequest, owner: User) {
        super(req, owner);

        this.services = req.services;
        this.status = req.status;
    }

    status: requestStatus;
    services: service[];
}