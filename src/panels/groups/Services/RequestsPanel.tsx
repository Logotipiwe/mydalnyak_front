import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import React, {ReactNode} from "react";
import {Button, Card, Cell, Counter, Div, Group, Panel, UsersStack} from "@vkontakte/vkui";
import Header from "@vkontakte/vkui/dist/components/Header/Header";
import RequestItem from "./RequestItem";
import ServicesPanelHeader from "./AppPanelHeader";
import {Icon12Add, Icon12Cancel, Icon12Verified, Icon16Add, Icon20Add} from "@vkontakte/icons";

@inject("RootStore")
@observer
class RequestsPanel extends React.Component<{ RootStore?: RootStore, id: string, header: ReactNode }, any> {
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.servicesStore;
        const {myRequests, requestsToMe} = store;
        return (
            <Panel id={"requests"}>
                {this.props.header}
                <Div>
                    <Group header={<Header mode="primary">Заявки на мои услуги</Header>}>
                        {requestsToMe.filter(r=>r.status === "OPENED").map(r =>
                            <RequestItem request={r} asServiceOwner={true}/>
                        )}
                    </Group>
                    <Group header={<Header mode="primary">Мои заявки</Header>}>
                        {myRequests.filter(r=>r.status === "OPENED").map(r =>
                            <RequestItem request={r}/>
                        )}
                    </Group>
                    <Group header={<Header mode="primary">Принятые заявки от меня</Header>}>
                        {myRequests.filter(r=>r.status === "FINISHED").map(r =>
                            <RequestItem request={r}/>
                        )}
                    </Group>
                    <Group header={<Header mode="primary">Принятые мной заявки</Header>}>
                        {requestsToMe.filter(r=>r.status === "FINISHED").map(r =>
                            <RequestItem request={r} asServiceOwner={true}/>
                        )}
                    </Group>
                </Div>
            </Panel>
        );
    }
}

export default RequestsPanel;