import {inject, observer} from "mobx-react";
import React, {ReactNode} from "react";
import {Button, Card, Cell, Group, Header, Panel} from "@vkontakte/vkui";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import RootStore from "../../../stores/RootStore";
import {Icon24Delete} from "@vkontakte/icons";
import SellsPanelHeader from "./SellsPanelHeader";

@inject("RootStore")
@observer
class CartPanel extends React.Component<{ RootStore?: RootStore, id: string, header: ReactNode }, any>{
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.sellsStore;
        const {categories, products} = store;
        return (<Panel id={"cart"}>
                {this.props.header}
                <Group header={<Header mode={"secondary"}>Корзина</Header>}>
                    <Div><Card mode={"tint"}>{store.cartFormatted.map(cartItem => {
                        const product = store.getProd(cartItem.id);
                        if (!product) return null;
                        return <Cell
                            before={<>({product.price * cartItem.amount}р)</>}
                            after={<Icon24Delete onClick={store.delCart.bind(null, cartItem.id)}/>}
                        >
                            {product.title} ({cartItem.amount} шт.)
                        </Cell>
                    })}
                        {store.cart.length
                            ? <Button onClick={store.newOrder}>Оформить заказ
                                ({store.cart.reduce((s, x) => s + x.price, 0)}p)</Button>
                            : null
                        }
                    </Card></Div>
                </Group>
            </Panel>
        );
    }
}
export default CartPanel;