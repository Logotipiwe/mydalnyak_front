import React, {ReactNode} from "react";
import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import {Card, Panel} from "@vkontakte/vkui";
import Header from "@vkontakte/vkui/dist/components/Header/Header";
import Button from "@vkontakte/vkui/dist/components/Button/Button";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import OrderItem from "./OrderItem";
import SellsPanelHeader from "./SellsPanelHeader";
import UpperPanel from "../UpperPanel";

@inject("RootStore")
@observer
class OrdersPanel extends React.Component<{ RootStore?: RootStore, id: string, header: ReactNode }, any> {
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.sellsStore;
        return (
            <Panel id={"orders"}>
                {this.props.header}
                <Card mode="tint">
                    <Header
                        mode={"secondary"}
                        aside={<Button onClick={store.fetchOrders}>Обновить</Button>}
                    >Я заказал</Header>
                    <Div>
                        {store.orders?.map((o, i) => (
                            <OrderItem order={o} key={i}/>
                        ))}
                    </Div>
                    <Header
                        mode={"secondary"}
                    >У меня заказали</Header>
                    <Div>{store.ordersToMe?.map((o, i) => (
                        <OrderItem order={o} key={i}/>
                    ))}</Div>
                </Card>
            </Panel>
        );
    }
}

export default OrdersPanel;