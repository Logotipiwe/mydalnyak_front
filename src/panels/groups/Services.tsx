import {inject, observer} from "mobx-react";
import RootStore from "../../stores/RootStore";
import React from "react";
import {
    Panel,
    View
} from "@vkontakte/vkui";
import ServicesPanelHeader from "./Services/AppPanelHeader";
import ServicesPanel from "./Services/ServicesPanel";
import RequestsPanel from "./Services/RequestsPanel";
import AnalyticsPanel from "./Services/AnalyticsPanel";
import AppPanelHeader from "./Services/AppPanelHeader";
import {Icon28ServicesOutline} from "@vkontakte/icons";

@inject("RootStore")
@observer
class Services extends React.Component<{ RootStore?: RootStore, id: activeStories }, any> {
    componentDidMount() {
        this.props.RootStore!.servicesStore.onServicesInit();
    }

    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.servicesStore;

        const header = <AppPanelHeader tabs={[
            rootStore.getTabObj("services", "Услуги", store.fetchServices),
            rootStore.getTabObj("requests", "Заявки", store.fetchRequests, store.requestsToMe.filter(r=>r.status === "OPENED").length),
            rootStore.getTabObj("analytics", "Аналитика", store.fetchAnalytics)
        ]} store={store} icon={<Icon28ServicesOutline/>}/>

        return (
            <View id={'services'} activePanel={store.selectedTab}>
                <ServicesPanel id={"services"} header={header}/>
                <RequestsPanel id={"requests"} header={header}/>
                <AnalyticsPanel id={"analytics"} header={header}/>
            </View>
        );
    }
}

export default Services;