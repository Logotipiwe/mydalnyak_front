import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import {Button, Card, Cell, UsersStack} from "@vkontakte/vkui";
import React from "react";
import ServiceRequest from "../../../stores/models/ServiceRequest";
import {Icon12Cancel, Icon12Verified, Icon24Cancel, Icon24Delete} from "@vkontakte/icons";

@inject("RootStore")
@observer
class RequestItem extends React.Component<{ RootStore?: RootStore, request: ServiceRequest, asServiceOwner?: boolean }, any> {
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.servicesStore;
        const r = this.props.request;
        if(this.props.asServiceOwner) return (
            <Card key={r.id} mode={"outline"} style={{marginBottom: 10}}><Cell

                before={r.owner.vkData?.photo_200 ? <UsersStack photos={[r.owner.vkData.photo_200]}/> : null}
                description={<>От {r.owner.vkData?.name} {r.owner.vkData?.lastName}</>}
                after={<>
                    <Button
                        mode={"commerce"}
                        style={{marginRight: 5}}
                        onClick={store.acceptRequest.bind(null, r)}
                    ><Icon12Verified/></Button>
                    {/*<Button mode={"destructive"}><Icon12Cancel/></Button>*/}
                </>}
            >Услуга: {r.services.filter(s=>s.owner_id === rootStore.fetchedUser?.vkId).map(s=>s.title).join("; ")}</Cell></Card>
        );
        return (
            <Card mode={"outline"} style={{marginBottom: 10}}><Cell
                description={<>
                    {r.createdDate}
                </>}
                after={<>
                    <Button
                        mode={"destructive"}
                        onClick={store.cancelRequest.bind(null, r)}
                    ><Icon12Cancel/></Button>
                </>}
            >
                <div style={{display: "flex", flexDirection: "column"}}>{r.services.map(s => {
                        const photo = rootStore.getUserById(s.owner_id)?.vkData?.photo_200;
                        return <Cell
                            description={r.status === "FINISHED" ? <span style={{color: "green"}}>Принят</span> : "Ожидание"}
                            before={<UsersStack photos={photo ? [photo] : undefined}/>}
                            after={s.price + 'p'}
                        >{s.title}</Cell>;
                    }
                )}</div>
            </Cell></Card>
        );
    }
}

export default RequestItem;