import React from 'react';
import Header from '@vkontakte/vkui/dist/components/Header/Header';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import RootStore from "../stores/RootStore";
import {inject, observer} from "mobx-react";
import Sells from "./groups/Sells";
import {
    Epic,
    Input,
    Panel,
    PanelHeaderSimple,
    PanelSpinner,
    SizeType,
    Snackbar,
    Tabbar,
    TabbarItem,
    View
} from "@vkontakte/vkui";
import {
    Icon24DownloadOutline,
    Icon28MarketOutline,
    Icon28ServicesOutline,
    Icon28ShoppingCartOutline, Icon28UserOutline
} from "@vkontakte/icons";
import UpperPanel from "./groups/UpperPanel";
import Services from "./groups/Services";
import Deliveries from "./groups/Deliveries";
import Profile from "./groups/Profile";

type Props = { RootStore?: RootStore };

@inject("RootStore")
@observer
class Home extends React.Component<Props, any> {
    constructor(p: Props) {
        super(p);
    }

    render() {
        const store = this.props.RootStore!;
        const {snackbar} = store;

        if (!store.isInited) return <>
            <PanelHeaderSimple>Мой Дальняк</PanelHeaderSimple>
            <PanelSpinner/>
        </>;

        return (<>
                <Epic activeStory={store.activeStory} tabbar={<Tabbar>
                    <TabbarItem
                        selected={store.activeStory === "profile"}
                        onClick={store.setActiveStory.bind(null, "profile")}
                        text="Профиль"
                    >
                        <Icon28UserOutline/>
                    </TabbarItem>
                    <TabbarItem
                        selected={store.activeStory === "sells"}
                        onClick={store.setActiveStory.bind(null, "sells")}
                        text="Покупки"
                    >
                        <Icon28MarketOutline/>
                    </TabbarItem>
                    <TabbarItem
                        selected={store.activeStory === "services"}
                        onClick={store.setActiveStory.bind(null, "services")}
                        text="Услуги"
                    >
                        <Icon28ServicesOutline/>
                    </TabbarItem>
                    <TabbarItem
                        selected={store.activeStory === "delivery"}
                        onClick={store.setActiveStory.bind(null, "delivery")}
                        text="Доставка"
                    >
                        <Icon28ShoppingCartOutline/>
                    </TabbarItem>
                </Tabbar>}>
                    <Sells id={"sells"}/>
                    <Services id={"services"}/>
                    <Deliveries id={"delivery"}/>
                    <Profile id={"profile"}/>
                </Epic>

                {!snackbar ? null : <Snackbar
                    onClose={store.onHideSnackBar}
                    duration={2000}
                >
                    <span style={{color: snackbar.isRed ? "red" : undefined}}>{snackbar.text}</span>
                </Snackbar>}
            </>
        );
    }
}

export default Home;
