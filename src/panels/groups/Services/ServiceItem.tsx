import React from "react";
import {Avatar, Button, Cell, Div, RichCell, UsersStack} from "@vkontakte/vkui";
import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import Service from "../../../stores/models/Service";
import {
    Icon24DeleteOutline,
    Icon24HideOutline,
    Icon24ShoppingCartOutline, Icon24ViewOutline,
    Icon28DeleteOutline,
    Icon28HideOutline,
    Icon28ShoppingCartOutline
} from "@vkontakte/icons";

@inject("RootStore")
@observer
class ServiceItem extends React.Component<{ RootStore?: RootStore, service: Service }, any>{
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.servicesStore;
        const service = this.props.service;
        const ownerName = service.owner.vkData?.name + " " + service.owner.vkData?.lastName;
        return (
            <RichCell
                after={service.price+"p"}
                text={service.description}
                caption={<UsersStack photos={service.owner.vkData ? [service.owner.vkData?.photo_200] : undefined}>{ownerName}</UsersStack>}
                actions={<>
                    <Button
                        mode={"commerce"}
                        onClick={store.createRequest.bind(null, [service.id])}
                    ><Icon24ShoppingCartOutline/></Button>
                    {service.isHidden
                        ? <Button
                            mode={"outline"}
                            onClick={store.showService.bind(null, service)}
                        ><Icon24ViewOutline/></Button>
                        : <Button
                            mode={"outline"}
                            onClick={store.hideService.bind(null, service)}
                        ><Icon24HideOutline/></Button>}
                </>}
            >
                <span style={{color: service.isHidden ? "red" : undefined}}>{service.title}</span>
            </RichCell>
        );
    }
}
export default ServiceItem;