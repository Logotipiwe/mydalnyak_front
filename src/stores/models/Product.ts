import User from "./User";
import Ownable from "./Ownable";
import {extendObservable, makeAutoObservable} from "mobx";

export default class Product extends Ownable implements product{
    constructor(product: product, owner: User) {
        super(product, owner);
        this.category = product.category;
        this.price = product.price;
        this.status = product.status;
        this.stockAmount = product.stockAmount;
    }

    category: category | null;
    price: number;
    status: string;
    stockAmount: number;
}