import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import {
    Button,
    Div,
    FormItem,
    Group,
    Header,
    Input,
    ModalPage,
    ModalPageHeader,
    Textarea,
    withModalRootContext
} from "@vkontakte/vkui";
import React from "react";
import Delivery from "../../../stores/models/Delivery";
import DeliveryOrderItem from "./DeliveryOrderItem";

@inject("RootStore")
@observer
class DeliveryModal extends React.Component<{
    RootStore?: RootStore,
    delivery: Delivery,
    id: any,
    updateModalHeight?: Function
}, any> {
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.deliveryStore;
        const selectedDelivery = this.props.delivery;
        const isPrincipalCourier = selectedDelivery.owner_id === rootStore.fetchedUser?.vkId;
        return (<>
                {(isPrincipalCourier
                    ? selectedDelivery.deliveryOrderList
                    : selectedDelivery.ordersByUser(rootStore.fetchedUser!.vkId)
                ).map(o =>
                    <DeliveryOrderItem
                        o={o}
                        key={o.id}
                    />
                )}
                <Group header={<Header>Добавить заказ</Header>}>
                    <FormItem top={"Товар"}>
                        <Input
                            value={store.addingDeliveryOrder.title}
                            onChange={e => store.setAddingDeliveryOrder({
                                ...store.addingDeliveryOrder,
                                title: e.target.value
                            })}
                        />
                    </FormItem>
                    <FormItem top={"Комментарий"}>
                        <Textarea
                            value={store.addingDeliveryOrder.description}
                            onChange={e => store.setAddingDeliveryOrder({
                                ...store.addingDeliveryOrder,
                                description: e.target.value
                            })}
                        />
                    </FormItem>
                    <FormItem top={"Количество"}>
                        <Input
                            value={store.addingDeliveryOrder.amount}
                            onChange={e => store.setAddingDeliveryOrder({
                                ...store.addingDeliveryOrder,
                                amount: e.target.value
                            })}
                        />
                    </FormItem>
                    <Div><Button
                        stretched
                        onClick={() => store.addDeliveryOrder(selectedDelivery)?.then(() => this.props.updateModalHeight!())}
                    >Добавить</Button></Div>
                </Group>
            </>
        );
    }
}

export default withModalRootContext(DeliveryModal);