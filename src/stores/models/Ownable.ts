import User from "./User";
import {action, makeAutoObservable, makeObservable, observable} from "mobx";

export default abstract class Ownable implements ownable{
    protected constructor(ownable: ownable, owner: User) {
        makeObservable(this, {
            id: observable,
            owner_id: observable,
            title: observable,
            photo: observable,
            createdDate: observable,
            description: observable,
            owner: observable,
            rating: observable,
            setRating: action.bound
        })

        this.createdDate = ownable.createdDate;
        this.description = ownable.description;
        this.id = ownable.id;
        this.owner_id = ownable.owner_id;
        this.photo = ownable.photo;
        this.title = ownable.title;
        this.owner = owner;
    }
    id: string;
    owner_id: number;
    title: string;
    photo: string | null;
    createdDate: string;
    description: string | null;

    owner: User;
    rating?: rating

    setRating(rating: rating){
        this.rating = rating;
    }
}