import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import React from "react";
import {Avatar, Button, Cell, RichCell} from "@vkontakte/vkui";
import Deliveries from "../Deliveries";
import Delivery from "../../../stores/models/Delivery";
import {Icon20Check, Icon20DeleteOutline, Icon20MarketOutline, Icon24ShoppingCartOutline} from "@vkontakte/icons";

@inject("RootStore")
@observer
class DeliveryItem extends React.Component<{ RootStore?: RootStore, delivery: Delivery, key: any }, any>{
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.deliveryStore;
        const d = this.props.delivery;

        let action;
        switch (d.deliveryStatus) {
            case "PUBLISHED":
                action = <Button
                    mode={"commerce"}
                    onClick={()=>store.boughtDelivery(d)}
                ><Icon24ShoppingCartOutline/></Button>;
                break;
            // case "DELIVERED":
            //     break;
            // case "PROCESSING":
            //     break;
            // case "CANCELLED":
            //     break;
            case "BOUGHT":
                action = <Button
                    mode={"commerce"}
                    onClick={()=>store.finishDelivery(d)}
                ><Icon20Check/></Button>;
                break;

        }
        return (
            <RichCell
                key={d.id}
                before={<Avatar src={d.owner.vkData?.photo_200}/>}
                caption={d.deliveryStatus !== "BOUGHT"
                    ? <>
                        <div>Заявки до: {store.timeFromLocaleDate(d.stopAcceptOrdersTime)}</div>
                        <div>Доставка: {store.timeFromLocaleDate(d.time)}</div>
                    </>
                    : <div>Товары куплены</div>
                }
                actions={<>
                    <Button onClick={() => store.selectDelivery(d)}>+ заказ</Button>
                    {action}
                    <Button
                        mode={"destructive"}
                        onClick={() => store.cancelDelivery(d)}
                    ><Icon20DeleteOutline/></Button>
                </>}
            >{d.place}</RichCell>
        );
    }

}
export default DeliveryItem;