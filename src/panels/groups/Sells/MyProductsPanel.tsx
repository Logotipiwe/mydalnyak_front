import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import React, {ReactNode} from "react";
import {CardGrid, Group, Header, Panel} from "@vkontakte/vkui";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import ProductItem from "./ProductItem";

@inject("RootStore")
@observer
class MyProductsPanel extends React.Component<{ RootStore?: RootStore, id: string, header: ReactNode }, any>{
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.sellsStore
        return (
            <Panel id={"myProds"}>
                {this.props.header}
                <Div>
                    <Group header={<Header mode={"secondary"}>Мои товары</Header>}>
                        <CardGrid size={"l"}>{store.myProducts
                            .map(p => <ProductItem product={p} key={p.id}/>)}</CardGrid>
                    </Group>
                </Div>
            </Panel>
        );
    }
}
export default MyProductsPanel;