import {inject, observer} from "mobx-react";
import React, {ReactNode} from "react";
import RootStore from "../../../stores/RootStore";
import {Button, Div, Group, Input, Panel, Search, Spinner, Textarea} from "@vkontakte/vkui";
import Header from "@vkontakte/vkui/dist/components/Header/Header";
import ServiceItem from "./ServiceItem";
import ServicesPanelHeader from "./AppPanelHeader";

@inject("RootStore")
@observer
class ServicesPanel extends React.Component<{ RootStore?: RootStore, id: string, header: ReactNode }, any> {
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.servicesStore;
        const addingService = store.addingServiceData;
        return (
            <Panel id={"services"}>
                {this.props.header}
                <Div>
                    <Group header={<Header
                        mode="primary">{!store.isSearchActive ? "Популярные услуги" : "Поиск по услугам"}</Header>}>
                        <Search
                            value={store.search}
                            onFocus={() => store.setIsSearchFocused(true)}
                            onBlur={() => store.setIsSearchFocused(false)}
                            onChange={e => store.setSearch(e.target.value)}
                        />
                        {(store.isSearchActive && store.foundServices.length)
                            ? <Div><Button
                                stretched
                                onClick={store.createRequest.bind(null, store.foundServices.map(s => s.id))}
                            >Отправить заявку на все найденные</Button></Div>
                            : null
                        }
                        {(store.isSearchActive ? store.foundServices : store.popularServices).map(s =>
                            <ServiceItem service={s} key={s.id}/>
                        )}
                        {store.isSearching ? <Spinner/> : null}
                    </Group>
                    <Group header={<Header mode={"primary"}>Добавить услугу</Header>}>
                        <div style={{display: "flex", flexDirection: "column", marginBottom: 10}}>
                            <div style={{display: "flex", marginBottom: 10}}>
                                <Input
                                    placeholder="Название"
                                    style={{display: "flex", flexGrow: 1}}
                                    value={addingService.title}
                                    onChange={e => store.setAddingService(
                                        {...addingService, title: e.target.value})}
                                />
                                <Input
                                    placeholder="Цена"
                                    style={{width: 100}}
                                    value={addingService.price}
                                    onChange={e => store.setAddingService(
                                        {...addingService, price: e.target.value})}
                                />
                            </div>
                            <Textarea
                                placeholder={"Комментарий"}
                                value={addingService.description}
                                onChange={e => store.setAddingService(
                                    {...addingService, description: e.target.value})}
                            />
                        </div>
                        <Button
                            mode={"commerce"}
                            stretched
                            size={"m"}
                            onClick={store.addService}
                        >Добавить</Button>
                    </Group>
                    <Group header={<Header mode={"primary"}>Мои услуги</Header>}>
                        {store.myServices.map(s =>
                            <ServiceItem service={s} key={s.id}/>
                        )}
                    </Group>
                </Div></Panel>
        );
    }
}
export default ServicesPanel;