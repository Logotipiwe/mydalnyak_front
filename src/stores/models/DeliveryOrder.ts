import Ownable from "./Ownable";
import User from "./User";
import {makeObservable} from "mobx";
import Delivery from "./Delivery";

export default class DeliveryOrder extends Ownable implements deliveryOrder{
    constructor(order: deliveryOrder, owner: User) {
        super(order, owner);

        this.amount = order.amount;
        this.deliveryOrderStatus = order.deliveryOrderStatus;
        this.price = order.price;

    }

    delivery?: Delivery;
    amount: number;
    deliveryOrderStatus: deliveryOrderStatus;
    price?: number;

    setDelivery(d: Delivery){
        this.delivery = d;
    }

    get isBought(){
        return this.deliveryOrderStatus === "BOUGHT";
    }
}