import React, {ReactNode} from "react";
import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import SellsPanelHeader from "./SellsPanelHeader";
import UpperPanel from "../UpperPanel";
import Header from "@vkontakte/vkui/dist/components/Header/Header";
import Div from "@vkontakte/vkui/dist/components/Div/Div";
import {Card, CardGrid, File, FormItem, Input, Panel} from "@vkontakte/vkui";
import {Icon16Add, Icon16DownloadOutline, Icon24Camera} from "@vkontakte/icons";
import Button from "@vkontakte/vkui/dist/components/Button/Button";
import Group from "@vkontakte/vkui/dist/components/Group/Group";
import ProductItem from "./ProductItem";

@inject("RootStore")
@observer
class ProductsPanel extends React.Component<{ RootStore?: RootStore, id: string, header: ReactNode }, any> {
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.sellsStore;
        const {products} = store;
        return (
            <Panel id={"products"}>
                {this.props.header}
                <Header mode={"secondary"}>Новый товар</Header>
                <Div style={{display: "flex", flexDirection: "column"}}>
                    <Input style={{display: "flex", flexGrow: 1}} placeholder={'Название'}
                           onChange={e => store.setProdData({
                               ...store.newProdData,
                               title: e.target.value
                           })}
                           value={store.newProdData.title}
                    />
                    <div style={{display: "flex"}}>
                        <File before={<Icon24Camera/>}
                              onChange={e => store.setUploadingPhoto(e)}>{store.uploadingPhoto ? (store.uploadingPhoto.name.substr(0,5)+"..") : "выбрать"}</File>
                        <Input style={{width: 100, display: "flex", flexGrow: 1}} placeholder={'цена'}
                               onChange={e => store.setProdData({
                                   ...store.newProdData,
                                   price: e.target.value
                               })}
                               value={store.newProdData.price}
                        />
                        <Button mode={"commerce"} onClick={store.newProduct}><Icon16Add/></Button></div>
                </Div>
                <Div><Card mode={"tint"}>
                    <Header mode={"secondary"}
                            aside={<Button
                                onClick={store.fetchProdsByCat.bind(null, undefined)}><Icon16DownloadOutline/></Button>}>
                        Товары
                    </Header>

                    <Div><Group>
                        <FormItem top={"Поиск по товарам"}>
                            <Input value={store.productsSearch} onChange={e=>store.setProdsSearch(e.target.value)}/>
                        </FormItem>
                        <CardGrid size={"l"}>{store.productsToShow
                            .map(p => <ProductItem product={p} key={p.id}/>)}</CardGrid>
                    </Group></Div>

                </Card></Div>
            </Panel>
        );
    }
}

export default ProductsPanel;