import {Cell, Counter, List, PanelHeader, PanelHeaderContext, Tabs, TabsItem} from "@vkontakte/vkui";
import {inject, observer} from "mobx-react";
import RootStore from "../../../stores/RootStore";
import React from "react";
import {Icon16Dropdown, Icon20ServicesOutline} from "@vkontakte/icons";

@inject("RootStore")
@observer
class SellsPanelHeader extends React.Component<{ RootStore?: RootStore }, any>{
    render() {
        const rootStore = this.props.RootStore!;
        const store = rootStore.sellsStore;
        const tabs: tabObj[] = [
            rootStore.getTabObj("products", "Товары", store.fetchProdsByCat),
            rootStore.getTabObj("orders", "Заказы", store.fetchOrders, store.ordersToMe?.length),
            rootStore.getTabObj("cart", "Корзина", ()=>{}, store.cart.length),
            rootStore.getTabObj("subscriptions", "Подписки", store.fetchNewProdSubs),
        ]
        const currTab = tabs.find(t=>t.key === store.selectedTab)!;
        return (<>
                <PanelHeader>
                    <Tabs>
                        <Icon20ServicesOutline style={{color: "var(--accent)"}}/>
                        <TabsItem
                            onClick={rootStore.setIsHeaderOpened.bind(null, !rootStore.isHeaderOpened)}
                            after={<Icon16Dropdown style={{
                                transform: `rotate(${rootStore.isHeaderOpened ? '180deg' : '0'}) translateY(1px)`
                            }}/>}
                        >{currTab.title}</TabsItem>
                    </Tabs>
                </PanelHeader>
                <PanelHeaderContext opened={rootStore.isHeaderOpened} onClose={()=>rootStore.setIsHeaderOpened(false)}>
                    <List>
                        {tabs.map(tab=>
                            <Cell
                                key={tab.key}
                                onClick={()=>{
                                    store.changeTab(tab.key)
                                    tab.onSelect();
                                }}
                                after={tab.counter ? <Counter mode={"prominent"}>{tab.counter}</Counter> : null}
                            ><span style={{color: store.selectedTab === tab.key ? "var(--accent)": undefined}}>{tab.title}</span></Cell>
                        )}
                    </List>
                </PanelHeaderContext>
            </>
        );
    }
}
export default SellsPanelHeader;